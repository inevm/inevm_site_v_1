var menu_item = document.getElementsByClassName('menu_item');
var stick = document.getElementsByClassName('stick'); 
var sub_menu = document.getElementsByClassName('sub_menu');

menu_item[0].addEventListener("mouseover", stick_hover_over, false);
menu_item[0].addEventListener("mouseout", stick_hover_out, false);


for (var i = 0; i < sub_menu.length; i++) {

  sub_menu[i].addEventListener("mouseover", stick_hover_over, false);
  sub_menu[i].addEventListener("mouseout", stick_hover_out, false);

}



function stick_hover_over(event){
  stick[0].style.background='#fff';
  stick[0].style.paddingBottom='20px';
  stick[0].style.boxShadow= '0px 10px 22px rgba(117, 206, 240, 0.25)';
  
}

function stick_hover_out(event){
  stick[0].style.background='#fff';
  stick[0].style.paddingBottom='0px';
  stick[0].style.boxShadow= '0px 10px 22px rgba(117, 206, 240, 0.25)';

}